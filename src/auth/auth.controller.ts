import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dto/register.dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/verify-govaa-token')
  async verifyGovaaToken(@Body('token') token: string) {
    return this.authService.verifyGovaaToken(token);
  }

  @Post('/login')
  async login(@Body('token') token: string) {
    return this.authService.login(token);
  }

  @Post('/register')
  async register(@Body() payload: RegisterDto) {
    return this.authService.register(payload);
  }
}
