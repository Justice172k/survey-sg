import { IsEmail, IsString } from 'class-validator';

export class RegisterDto {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsString()
  agency: string;

  @IsString()
  description: string;

  @IsEmail()
  govEmail: string;
}
