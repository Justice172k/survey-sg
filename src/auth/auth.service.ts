import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import axios from 'axios';
import { UserService } from 'src/user/user.service';
import { CusError } from 'src/utils/errors/CusError';
import { ErrorCode } from 'src/utils/errors/error-code.enum';
import { ErrorMessage } from 'src/utils/errors/error-message';
import { RegisterDto } from './dto/register.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async verifyGovaaToken(token: string) {
    const { data } = await axios.post('http://localhost:8080/api/verify', {
      token,
    });
    return data;
  }

  async login(token: string) {
    const verifyToken = await this.verifyGovaaToken(token);
    if (!verifyToken.success) {
      throw new CusError(ErrorCode.INVALID_TOKEN, ErrorMessage.INVALID_TOKEN);
    }
    const { email } = verifyToken.payload;
    const user = await this.userService.findUserByEmail(email);

    if (!user) {
      throw new CusError(ErrorCode.USER_NOT_FOUND, ErrorMessage.USER_NOT_FOUND);
    }
    const accessToken = this.jwtService.sign(
      {
        userId: user.id,
        govEmail: user.govEmail,
      },
      {
        expiresIn: `${this.configService.get('EXPIRES_ACCESS_TOKEN')}d`,
        secret: this.configService.get('JWT_SECRET'),
      },
    );
    return {
      accessToken,
    };
  }

  async register(payload: RegisterDto) {
    const user = await this.userService.createUser(payload);
    const accessToken = this.jwtService.sign(
      {
        userId: user.id,
        govEmail: user.govEmail,
      },
      {
        expiresIn: `${this.configService.get('EXPIRES_ACCESS_TOKEN')}d`,
        secret: this.configService.get('JWT_SECRET'),
      },
    );
    return {
      accessToken,
    };
  }
}
