export const ErrorMessage = {
  EMAIL_ALREADY_EXIST: 'email already exists',
  USER_NOT_FOUND: 'user not found',
  INVALID_TOKEN: 'invalid token',
};
