import { HttpStatus } from '@nestjs/common';

export enum ErrorCode {
  EMAIL_ALREADY_EXIST = HttpStatus.CONFLICT,
  USER_NOT_FOUND = HttpStatus.NOT_FOUND,
  INVALID_TOKEN = HttpStatus.UNAUTHORIZED,
}
