import { ErrorCode } from './error-code.enum';

export class CusError extends Error {
  constructor(public code: ErrorCode, message: string) {
    super();
    this.message = message;
    this.code = code;
  }
}
