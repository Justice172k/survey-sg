import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { HttpArgumentsHost } from '@nestjs/common/interfaces/features/arguments-host.interface';
import { Response } from 'express';
import { QueryFailedError } from 'typeorm';
import { CusError } from '../CusError';

@Catch()
export class AllExceptionFilter implements ExceptionFilter {
  private static handleResponse(
    response: Response,
    exception: HttpException | QueryFailedError | CusError | Error,
  ): void {
    let responseBody: any = { message: 'Internal server error' };
    let statusCode = HttpStatus.INTERNAL_SERVER_ERROR;

    if (exception instanceof Error) {
      responseBody = {
        statusCode: statusCode,
        message: exception.stack,
      };
    }

    if (exception instanceof HttpException) {
      responseBody = exception.getResponse();
      statusCode = exception.getStatus();
    } else if (exception instanceof QueryFailedError) {
      statusCode = HttpStatus.BAD_REQUEST;
      responseBody = {
        statusCode: statusCode,
        message: exception.message,
      };
    } else if (exception instanceof CusError) {
      responseBody = {
        statusCode: exception.code,
        message: exception.message,
      };
    }

    response.status(statusCode).json(responseBody);
  }

  catch(
    exception: HttpException | CusError | Error,
    host: ArgumentsHost,
  ): void {
    const ctx: HttpArgumentsHost = host.switchToHttp();
    const response: Response = ctx.getResponse();

    // Handling error message and logging
    this.handleMessage(exception);

    // Response to client
    AllExceptionFilter.handleResponse(response, exception);
  }

  private handleMessage(
    exception: HttpException | QueryFailedError | Error | CusError,
  ): void {
    let message = 'Internal Server Error';

    if (exception instanceof Error) {
      message = exception.stack.toString();
    }

    if (exception instanceof HttpException) {
      message = JSON.stringify(exception.getResponse());
    }
    if (exception instanceof QueryFailedError) {
      message = exception.stack.toString();
    }

    if (exception instanceof CusError) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      message = exception.stack.toString();
    }
  }
}
