import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/guard/auth.guard';
import { Users } from './entity/user.entity';
import { UserService } from './user.service';

@UseGuards(AuthGuard)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/current-user')
  async getCurrentUser(
    @Request() req: Request & { govEmail: string },
  ): Promise<Users> {
    return this.userService.findUserByEmail(req.govEmail);
  }
}
