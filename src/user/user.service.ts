import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CusError } from 'src/utils/errors/CusError';
import { ErrorCode } from 'src/utils/errors/error-code.enum';
import { ErrorMessage } from 'src/utils/errors/error-message';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { Users } from './entity/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(Users) private readonly userModel: Repository<Users>,
  ) {}

  async createUser(payload: CreateUserDto): Promise<Users> {
    const user: Users = await this.userModel.findOne({
      where: {
        govEmail: payload.govEmail,
      },
    });
    if (user) {
      throw new CusError(
        ErrorCode.EMAIL_ALREADY_EXIST,
        ErrorMessage.EMAIL_ALREADY_EXIST,
      );
    }
    const createdUser = this.userModel.create(payload);
    return await this.userModel.save(createdUser);
  }

  async findUserByEmail(email: string): Promise<Users> {
    const user: Users = await this.userModel.findOne({
      where: {
        govEmail: email,
      },
    });
    return user;
  }
}
