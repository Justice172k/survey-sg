import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  name: string;

  @Column({
    unique: true,
  })
  email: string;

  @Column({
    nullable: false,
  })
  agency: string;

  @Column({
    nullable: false,
  })
  description: string;

  @Column({
    unique: true,
    nullable: false,
  })
  govEmail: string;
}
